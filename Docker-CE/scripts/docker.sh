#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
sudo apt-get purge docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt upgrade -y
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg2
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install -y docker-ce
sudo usermod -a -G docker ubuntu
#Adding docker version output:
docker --version
