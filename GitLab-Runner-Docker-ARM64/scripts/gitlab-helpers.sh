#!/bin/bash


docker run -d -p 6000:5000 \
    -e REGISTRY_PROXY_REMOTEURL=https://registry-1.docker.io \
    --restart always \
    --name registry registry:2

# Minio is NOT compiled to run on ARM64. Please see this project for more information: 
# https://gitlab.com/gitlab-com/alliances/aws/sandbox-projects/minio-arm64

echo "grab ARM64 minio from GitLab container registry:"

docker run -d -p 9005:9000 \
    -v /.minio:/root/.minio -v /export:/export \
    --name minio \
    --restart always \
    registry.gitlab.com/gitlab-com/alliances/aws/sandbox-projects/minio-arm64:latest \
    server /export

sudo mkdir /export/runner

# Docker may take a second to respond.
# sleep 5

# while loop to check for running images:

while :
do
	ready=`docker ps | grep [minio,registry] | wc -l`
    echo $ready
	if [ $ready -eq 2 ]
	then
		break
	fi
	loops=$((loops+1))
	echo $loops
    if [ $loops -eq 1000 ]
    then 
        break
    fi
done