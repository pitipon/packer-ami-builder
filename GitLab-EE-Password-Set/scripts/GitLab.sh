#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
apt update
apt upgrade -y
apt install openssh-server ca-certificates python3-pip ansible -y
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
mkdir -p /etc/gitlab
cd /etc/gitlab
wget https://gitlab.com/gitlab-org/omnibus-gitlab/-/raw/master/files/gitlab-config-template/gitlab.rb.template
sed "s/^external_url.*/external_url 'http:\/\/test.com'/g" gitlab.rb.template > gitlab.rb
echo "gitlab_rails['initial_root_password'] = \"gitlab1234\"" >> gitlab.rb
rm gitlab.rb.template
apt-get install gitlab-ee

